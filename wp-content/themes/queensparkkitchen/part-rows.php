<?php 
$queried_object = get_queried_object(); 


if ( have_rows('rows', $queried_object) ) {

	while ( have_rows('rows', $queried_object) ) : the_row();
		if( get_row_layout() == 'chess_board' ) {
			
			if( have_rows('chess') ) {
				$i = 0;
				
				while ( have_rows('chess') ) : the_row();
					$i++;
					if ($i % 2 == 0) {
						$imagefloat = 'left';
					} else {
						$imagefloat = 'right';
					}
					$image1 = get_sub_field('image1');
					$image2 = get_sub_field('image2');
					$image3 = get_sub_field('image3');
					
					$heading = get_sub_field('heading');
					$text = get_sub_field('text');
					$buttonlink = get_sub_field('buttonlink');
					$buttontext = get_sub_field('buttontext');
					?>
					<section class="line contentblock-<?php echo $imagefloat; ?> <?php echo $imagefloat; ?>">
						<div class="imageblock">
							<div class="mainimage" style="background-image: url(<?php echo $image1; ?>); background-position: center center; background-size: cover;">
							</div>
							<div class="restimages mobile-hide">
								<div class="restimageone" style="background-image: url(<?php echo $image2; ?>); background-position: center center; background-size: cover;">
								</div>
								<div class="restimagetwo" style="background-image: url(<?php echo $image3; ?>); background-position: center center; background-size: cover;">
								</div>
							</div>
						</div>
						<div class="info">
							<?php if( get_sub_field('heading') ): ?><h2><?php echo $heading; ?></h2><?php endif; ?>
							<?php if( get_sub_field('text') ): ?><p><?php echo $text; ?></p><?php endif; ?>
							<?php if( get_sub_field('buttonlink') ): ?>
							<a href="<?php echo $buttonlink; ?>" id="buttonspecial" class="<?php echo $buttontext; ?>"></a>
							<?php endif; ?>
						</div>
					</section>
				<?php endwhile;
			}
					
		} else if ( get_row_layout() == 'single_column' ) {
			$heading = get_sub_field( 'heading' );
			$text = get_sub_field( 'text' );
			$buttonlink = get_sub_field('buttonlink');
			$buttontext = get_sub_field('buttontext');
			?>
			<section id="singlecolumn" class="line column">
				<?php if( get_sub_field('heading') ): ?><h1><?php echo $heading; ?></h1><?php endif; ?>
				<?php if( get_sub_field('text') ): ?><p><?php echo $text; ?></p><?php endif; ?>
				<?php if( get_sub_field('buttonlink') ): ?><a href="<?php echo $buttonlink; ?>" id="buttonspecial" class="<?php echo $buttontext; ?>"></a><?php endif; ?>
			</section>
			
			<?php 
		}
		
    endwhile;

} ?>
