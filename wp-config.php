<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/Users/Sander/Sites/queensparkkitchen.local/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'queensparkkitchen_local');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pfu4)?|IKYvJ;)%0&3K.s^?%*mL4DA}yOa%3-xk*$=tN&Ug&ZEJDE34M/QlqSUfo');
define('SECURE_AUTH_KEY',  '=%m<eJ~70IC:dnxr+u%W4-)LoPN<=~m22.OcN4&R~kR!TG,>=.,6438tv]]S7}7d');
define('LOGGED_IN_KEY',    '_6k:o,>zH{Lv[:tLX}m*eQF(](tI pDN)aHX_]Lbj[/+^+T%6n|e|#Go~&Y7U(cg');
define('NONCE_KEY',        '=k +TF)<9t4/nVvldIKpCS_!/gxB 0CYmY6h q9+S+*9&.o!Jj:Gch?UQ &7M;f_');
define('AUTH_SALT',        '8q&Kz(K=@b^$.$yrl`7$,aN}N:jjsa$J%cxe@YUQ<CjN*)wt(}N %I6$nS_=u3u$');
define('SECURE_AUTH_SALT', 'iy3z|q0k[)m&CyE3[goG1f:xR5K[!cWU$$jRMP&N$7E0u5<XuCYkIsG7oABi]|e<');
define('LOGGED_IN_SALT',   's@nA!~{o%i0?%l*U[x}+-YK&c[[ ZW$Dg7,&<qMP!-*+rC@>`571C-F|@fOud_Jd');
define('NONCE_SALT',       'A1D~-A/ B8t$%_Z:2Nic=JhLI1z*R(v0_K!61V8t,w#-8/mK$.<aD0H6S(sc-*{0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'qpk_wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define( 'WP_DEBUG_LOG', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
