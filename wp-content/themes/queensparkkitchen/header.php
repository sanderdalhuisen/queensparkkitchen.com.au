<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> id="backtotop">
	<header>
		<div class="park center navigationbar">
				<?php wp_nav_menu( array(
					'container' => 'nav',
					'theme_location' => 'center',
					'menu_id'        => 'center',
				) ); ?>
		<div class="socialassets">
			<a target="_blank" href="https://facebook.com/queensparkkitchen"><img src="<?php bloginfo('template_directory'); ?>/images/facebook.png" alt="Facebook" id="sociallogo" /></a>
			<a target="_blank" href="https://instagram.com/queensparkkitchen"><img src="<?php bloginfo('template_directory'); ?>/images/instagram.png" alt="Instagram" id="sociallogo" /></a>
		<div class="clear"></div>
		</div>
		</div>
		<div class="park center" id="header">
			<img src="<?php bloginfo('template_directory'); ?>/images/QPK_GraphicHeader.png" alt="Tree Branches" class="treebranches" />
			<a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/QPK_MasterLogo_Pink.png" alt="Logo Queens Park Kitchen" class="logo" /></a>	
		</div>
	</header>