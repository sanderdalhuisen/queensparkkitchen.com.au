<footer>
	<section class="line">
		<div class="contentblock-footer">
			<div class="footercontent">
				<div id="left">
				</div>
				<div id="right">
					<h2>Your home away from home</h2>
				</div>
			</div>
			<div class="footercontent">
				<div id="left">
					<p>Location</p>
				</div>
				<div id="right">
					<p>Darley Rd, Queens Park, NSW 2031</p>
				</div>
			</div>
			<div class="footercontent">
				<div id="left">
					<p>We are open</p>
				</div>
				<div id="right">
					<p>Every day 7am - 4pm</p>
					<p>Friday & Saturday Baracca BBQ open for dinner</p>
				</div>
			</div>
			<div class="footercontent">
				<div id="left">
					<p>Contact</p>
				</div>
				<div id="right">
					<p>02 9380 9350</p>
					<p>hello@queensparkkitchen.com.au</p>
				</div>
			</div>
			<div class="footercontent social">
				<div id="left"></div>
				<div id="right">
					<p style="display: inline; padding-right: 25px;"><a target="_blank" href="https://facebook.com/queensparkkitchen/">facebook</a></p>
					<p style="display: inline; padding-right: 25px;"><a target="_blank" href="https://instagram.com/queensparkkitchen/">instagram</a></p>
					<p style="display: inline;"><a href="#modalnewsletter" class="newsletter">newsletter</a></p>
					<a href="#backtotop" id="buttonspecial" class="backtotop scrollbutton"></a>
				</div>
			</div>
		</div>

		<div id="footersectionlogo">
			<div class="park center" class="footer">
			<a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/QPK_MasterLogo_Pink.png" alt="Logo Queens Park Kitchen" class="logo" /></a>
		</div>
	</section>
<img src="<?php bloginfo('template_directory'); ?>/images/tree.png" alt="Tree" class="footertreetwo" />
<p class="partofTWG">A part of Trippas White Group</p>
</footer>
<?php wp_footer(); ?>

<div id="modal" style="display: none; position: fixed; top: 0; left: 0; z-index: 100000; width: 100vw; height: 100vh; overflow: scroll; padding: 50px; background: rgba(0, 0, 0, 0.5);">
	<div class="border">
	<a href="#close" class="modal__close"><img src="<?php bloginfo('template_directory'); ?>/images/Close.png" alt="close button" class="closebutton" /></a>
	<div class="modal__content" style="margin: auto; background: #f4c5c3;">
		<img src="<?php bloginfo('template_directory'); ?>/images/QPK_LogoMark_Green%20(1).png" alt="close button" class="modallogo" />
		<p class="modaltext">event enquiry form</p>
		<div id="trippas__respak" data-venue="QPK" data-event-types="Wedding bookings|Corporate Events|Social Events"></div>
		<script src="https://www.trippaswhitegroup.com.au/respak/app/dist/static/js/app.js"></script>
	</div>
	</div>
</div>

<script>
jQuery(document).ready(function() {
	jQuery('.enquiry').on('click', function (e) {
		e.preventDefault();
		jQuery('#modal').toggleClass('show');
	});
	jQuery('.modal__close').on('click', function (e) {
		e.preventDefault();
		jQuery('#modal').removeClass('show');
	});
});
</script>

<div id="modalnewsletter" style="display: none; position: fixed; top: 0; left: 0; z-index: 100001; width: 100vw; height: 100vh; background: rgba(0, 0, 0, 0.5);">
<div class="border">
	<a href="#close" class="modal__close"><img src="<?php bloginfo('template_directory'); ?>/images/Close.png" alt="close button" class="closebutton" /></a>
	<div class="modal__content" style="margin: auto; background: #f4c5c3;">
		<img src="<?php bloginfo('template_directory'); ?>/images/QPK_LogoMark_Green%20(1).png" alt="close button" class="modallogo" />
		<p class="modaltext">Sign up for our newsletter</p>
		<script src="//myguestlist.com.au/mgl/displayform.php?formID=mf5b6ce3a7c59f6" type="text/javascript"></script>
	</div>
</div>
</div>

<script>
jQuery(document).ready(function() {
	jQuery('.newsletter').on('click', function (e) {
		e.preventDefault();
		jQuery('#modalnewsletter').toggleClass('show');
	});
	jQuery('.modal__close').on('click', function (e) {
		e.preventDefault();
		jQuery('#modalnewsletter').removeClass('show');
	});
});
</script>

<script>
jQuery(document).ready(function(){
  // Add smooth scrolling to all links
  jQuery(".backtotop").on('click', function(event) {

    if (this.hash !== "scrollbutton") {
      event.preventDefault();

      var hash = this.hash;

      jQuery('html, body').animate({
        scrollTop: jQuery(hash).offset().top
      }, 800, function(){
	      
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

</body>
</html>
