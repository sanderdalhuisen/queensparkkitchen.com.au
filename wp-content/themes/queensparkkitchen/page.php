<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post();
		$rows = get_field( 'rows' ); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="line center park">
				<div class="entry-content">
					<?php get_template_part('part', 'rows'); ?>
			
						<?php wp_link_pages( array(
							'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
							'after'  => '</div>',
						) );
					?>
				</div><!-- .entry-content -->
			</div>
		</article><!-- #post-## -->
		
	<?php endwhile; ?>
<?php get_footer();