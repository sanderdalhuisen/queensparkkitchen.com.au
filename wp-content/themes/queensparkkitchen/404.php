<?php get_header(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<section class="line center park">
				<div class="entry-content">
				<section id="singlecolumn" class="line column" style="padding-bottom: 250px;">
				<h2>Oops, it looks like you got a bit lost!</h2>
				<p>It sometimes happens. Just know that we're there to make sure you get back home save.</p>
				<a href="<?php echo get_home_url(); ?>" id="buttonlinktwo" style="margin: 50px auto 0;">take me home</a>
				<img src="<?php bloginfo('template_directory'); ?>/images/Menu-2017-53.jpg " alt="Menu image	" style="padding-top: 100px;" />
			</section>
				</div><!-- .entry-content -->
			</section>
		</article><!-- #post-## -->
<?php get_footer();